package com.treino.biblioteca.service;

import com.treino.biblioteca.model.Livro;
import com.treino.biblioteca.repository.LivroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LivroService {

    @Autowired
    private LivroRepository livroRepository;

    public Livro insert(Livro livro) {
        return livroRepository.save(livro);
    }

    public List<Livro> findAll() {
        return livroRepository.findAll();
    }

    public Livro findById(Long id) {
        Optional<Livro> livro = livroRepository.findById(id);
        return livro.get();
    }

    public Livro findByTitulo(String titulo) {
        Livro livro = livroRepository.findByTituloIgnoreCaseContaining(titulo);
        return livro;
    }

    public void delete(Long id) {
        livroRepository.deleteById(id);
    }

    public Livro update(Livro livro) {
        Livro updateLivro = findById(livro.getId());
        updateData(updateLivro, livro);
        return livroRepository.save(updateLivro);
    }

    private void updateData(Livro updateLivro, Livro livro) {
        updateLivro.setTitulo(livro.getTitulo());
        updateLivro.setAutor(livro.getAutor());
    }
}