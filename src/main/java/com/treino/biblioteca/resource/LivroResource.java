package com.treino.biblioteca.resource;

import com.treino.biblioteca.model.Livro;
import com.treino.biblioteca.service.LivroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/livros")
public class LivroResource {

    @Autowired
    private LivroService livroService;

    @PostMapping
    public ResponseEntity<Livro> insert(@RequestBody Livro livro) {
        livro = livroService.insert(livro);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(livro.getId()).toUri();
        return ResponseEntity.created(uri).body(livro);
    }

    @GetMapping
    public ResponseEntity<List<Livro>> findAll() {
        List<Livro> list = livroService.findAll();
        return ResponseEntity.ok().body(list);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Livro> findById(@PathVariable Long id) {
        Livro obj = livroService.findById(id);
        return ResponseEntity.ok().body(obj);
    }

    @GetMapping(value = "/findByTitulo/{titulo}")
    public ResponseEntity<Livro> findByTitulo(@PathVariable String titulo) {
        Livro obj = livroService.findByTitulo(titulo);
        return ResponseEntity.ok().body(obj);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        livroService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Void> update(@RequestBody Livro livro, @PathVariable Long id) {
        Livro livroUpdate = livroService.update(livro);
        return ResponseEntity.noContent().build();
    }

}
