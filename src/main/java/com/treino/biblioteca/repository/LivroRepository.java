package com.treino.biblioteca.repository;

import com.treino.biblioteca.model.Livro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface LivroRepository extends JpaRepository<Livro, Long>, PagingAndSortingRepository<Livro, Long > {

    Livro findByTituloIgnoreCaseContaining(String titulo);
}
